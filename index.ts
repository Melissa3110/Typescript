let var1: boolean;
var1 = true;

//005.Typescript - basic types
//string = type caractère( après let)
//arrow = nombre( après let )

let arr:string[];
arr=[];
arr.push("ok");



let arr1:number[];
arr1=[];
arr1.push(4);


//Les Objet defintion( objet ou age))

let obj:{name:string};
obj={name:"Jean"};

let obj2:{name1:string,age:number};
// @ts-ignore
obj2={name1:`JiJi`,age:"14"};

//011.Typescript -interfaces
//
interface IVehicule {
    model: string;
    km:number;
    move(): string;
    marque?: string;
}

    let car:(IVehicule);

car={

    model:'BMW',
    km:12,
    move:function(){
        return'';
        },
marque:"ok"
    };

//006.Typescript - fonctions

//006.Typescript - functions


//007.Typescript - enums
 enum Direction {
     TOP ="1233",
     BOTTOM ="327",
     RIGHT ="567",
     LEFT ="234",
}

let userDirection: Direction =Direction.TOP;
 console.log(userDirection);


//012.Typescript - class

//
 class Car {
    model: string;

constructor (model:string){
    this.model=model; }
}
let car2 = new Car ('RS3');




//010.Typescript - types assertions

 let input: unknown= "24";    // Formulaire page HTML
 let var2: number;

var2 = input as number ;



//009.Typescript - literal types


let transition:"ease-in"|"ease-out";

 transition:"ease";

function transformX(transition:string): void {

}
    transformX:(transition);


//006.Typescript - functions

//
function createUser (name:string,age:number,adress?:string):
{name:string,age:number,adress:string} {

   return {
       name,
       age,
       adress

   };

}

let fn :(nb:string)=>boolean|number;

createUser( 'Jean',  12, "Bld");